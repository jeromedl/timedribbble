class ActiveSupport::Logger::SimpleFormatter
  SEVERITY_TO_COLOR_MAP   = {'DEBUG'=>'32', 'INFO'=>'37', 'WARN'=>'33', 'ERROR'=>'31', 'FATAL'=>'31', 'UNKNOWN'=>'37'}
 
  def call(severity, time, progname, msg)

    # formatted_severity = sprintf("%-5s",severity)
    formatted_time = time.strftime("%Y-%m-%d %H:%M:%S.") << time.usec.to_s[0..2].rjust(3)
    color = SEVERITY_TO_COLOR_MAP[severity]
    # (pid:#{$$})
    "#{formatted_time} \033[#{color}m[#{severity}] #{msg.strip} \033[0m\n"
  end
end