class AddUidToVisitors < ActiveRecord::Migration
  def change
    add_column :visitors, :uid, :string
  end
end
