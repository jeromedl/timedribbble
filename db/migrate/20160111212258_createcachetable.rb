class Createcachetable < ActiveRecord::Migration
  def change
    create_table :yearimages do |t|
      t.string :year
      t.string :image
      t.string :backlink
      t.timestamps null: false
    end
  end
end
