include ApplicationHelper

class StaticController < ApplicationController

  def home
    fetch_shots
  end

  def save_email
    v = Visitor.new
    if !params["email"].blank? && !Visitor.find_by(email:params["email"])
      v.email = params["email"]
      v.uid = SecureRandom.urlsafe_base64
      v.save
      Rails.logger.info "new subscription: #{v.inspect}"
    else
      Rails.logger.error "bad request #{params}"
    end
    head :ok
  end

  def unsubscribe
    uid = params[:uid]
    if uid
      visitor = Visitor.find_by(uid:uid)
      if visitor
        Rails.logger.warn "unsubscribing: #{visitor.inspect}"
        visitor.destroy!
      end
    end
  end
end
