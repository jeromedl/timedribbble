include ApplicationHelper

class ShotMailer < ApplicationMailer
  default from: 'Timetrabbble <jeromedl@zeroheight.com>'

  def daily_email(visitor)
    fetch_shots
    @uid = visitor.uid
    email = visitor.email
    Rails.logger.info "creating daily_email to #{email}"
    attachments.inline['header.png'] = File.read('public/images/header.png')
    mail(to: email, subject: 'Daily inspiration')
  end

end
