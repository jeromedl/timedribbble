require 'open-uri'
require 'json'

module ApplicationHelper
    def fetch_shots
    reload_cache = false
    cache = Yearimages.find_by(year:"2011")
    if not cache
      Rails.logger.warn "No cache, needs reload"
      reload_cache = true
    else
      if not cache.updated_at.today?
        Rails.logger.warn "Cache older than today: #{cache.updated_at}, needs reload"
        reload_cache = true
      end
    end

    @urls = {}

    now = Time.now
    @date = now.strftime "%-d %B"
    yday = now.yesterday
    if reload_cache
      Rails.logger.warn "Reloading cache"
      Yearimages.destroy_all
      years = (2011..2016)      
      years.each { |year|
        url = "https://api.dribbble.com/v1/shots?date=#{year}-#{yday.month}-#{yday.day}&access_token=d92afb6c5a0b5a4d5f04ae879c6a45bac1208dfa29cb98564036e2bf78073550"
        html = open(url)
        json_html = JSON.parse(html.read)
        year_json = {}
        most_popular_shot = json_html.first
        if most_popular_shot
          if most_popular_shot["images"]
            year_json["image"] = most_popular_shot["images"]["normal"]
          end
          year_json["backlink"] = most_popular_shot["html_url"]
        end

        Yearimages.create!(year: year, 
          image:most_popular_shot["images"]["normal"],
          backlink:most_popular_shot["html_url"])

        @urls[year] = year_json
      }
    else
      Rails.logger.warn "Hit the cache"
      yis = Yearimages.all
      yis.each do |yearimage|
        year_json = {}
        year_json['year'] = yearimage.year
        year_json['image'] = yearimage.image
        year_json['backlink'] = yearimage.backlink
        @urls[yearimage.year] = year_json
      end
    end
  end

end
