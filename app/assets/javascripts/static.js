// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.


function validateEmail(email) 
{
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}

function feedback(msg) {
	var submit = $(".dose-btn");
	submit.val(msg);
	submit.addClass("pink");
	submit.removeClass("blue");
	submit.addClass("shake-rotate shake-constant")
	setTimeout(function(){
		submit.removeClass("shake-rotate shake-constant")
		submit.val("INSPIRE ME");
		submit.addClass("blue");
		submit.removeClass("pink");
	}, 1000);
}

$(function() {
	$('form').submit(function() {
		var email_field = $("#email_field");
		var email = email_field.val();
		if (email && email.length && validateEmail(email)) {
			$.post("email", {
				email: email
			}, function() {
				email_field.val("");
				feedback("YOU GOT IT!");
			});
		}
		else {
			feedback("INVALID EMAIL")
		}
	});
});