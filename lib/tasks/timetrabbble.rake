namespace :timetrabbble do

  desc "send daily emails to subscribers"
  task :send_emails => :environment do
    Visitor.all.each_with_index do |subscriber, i|
      begin
        if i % 45 == 0
          puts "sent 45 emails, sleeping 60 minutes"
          sleep 3600
        end
        ShotMailer.daily_email(subscriber).deliver
        sleep 3
      rescue => e
        puts "error sending to #{subscriber.email}: #{e}"
      end
    end
    puts "\n[#{Time.now}]\nEmail sent (attempts) to #{Visitor.count} subscribers\n"
  end

  desc "send test preview emails to jdelafargue@gmail.com"
  task :send_test_email_jerome => :environment do
    if defined?(Rails) && (Rails.env == 'development')
      Rails.logger = Logger.new(STDOUT)
    end
    v = Visitor.new
    v.email = "jdelafargue@gmail.com"
    v.uid = "jdelafargue@gmail.com"
    begin
      sent = ShotMailer.daily_email(v).deliver
      puts "\n[#{Time.now}]\nEmail sent to #{v.inspect}\n"
    rescue => e
      puts "error sending to #{v.email}: #{e}"
    end
  end

  desc "backfill uids"
  task :backfill_uids => :environment do
    Visitor.all.each do |subscriber|
      if !subscriber.uid
        subscriber.uid = SecureRandom.urlsafe_base64
        subscriber.save!
      end
    end
  end

end
